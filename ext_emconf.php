<?php

$EM_CONF[$_EXTKEY] = [
	'title' => 'spamshield',
	'description' => 'Universal invisible Spamshield for TYPO3',
	'category' => 'fe',
	'shy' => 0,
	'version' => '7.0.2',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearcacheonload' => 0,
	'lockType' => '',
	'author' => 'Ronald P. Steiner, Hauke Hain, Christian Seifert',
	'author_email' => 'ronald.steiner@googlemail.com, fearistic@gmail.com, christian-f-seifert@gmx.de',
	'author_company' => '',
	'CGLcompliance' => NULL,
	'CGLcompliance_note' => NULL,
	'constraints' =>
		[
			'depends' =>
				[
					'typo3' => '7.6.0-8.7.99',
				],
			'conflicts' => '',
			'suggests' =>
				[],
		],
];
